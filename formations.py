from math import atan2

import numpy

from Rectangle import Rectangle, Point
from model.Game import Game
from model.Move import Move
from model.Player import Player
from model.VehicleType import VehicleType
from model.World import World
from processes import MoveRectangleFormationProcess, add_process_to_queue, SplitRectangleFormationProcess, get_vehicles, \
    get_vehicle, SpreadFormationProcess, AssignToGroup, RotateFormationProcess


norm_vector = numpy.array([10, 0], dtype=float)


class FormationState:
    NONE = 0
    FORMAT = 1
    JUST_CREATED = 2
    DISPERSED = 3
    FIGHTING = 4
    ROTATE = 5
    MOVE = 6
    ESCAPE = 7


class Formation:
    def __init__(self, units, world, vehicle_types, group, speed, min_range, max_range):
        self.min_range = min_range
        self.max_range = max_range
        self.speed = speed
        self.group = group
        self.vehicle_types = vehicle_types
        self.units = units
        self.state = FormationState.JUST_CREATED
        self.destination = self.coordinates(world)
        self.escape_point = None
        self.escape_tick = None

    def format(self, me: Player, world: World, game: Game, move: Move):
        raise NotImplemented

    def move(self, me: Player, world: World, game: Game, move: Move, point, is_vector=False):
        self.state = FormationState.MOVE
        formation_rectangle = self.formation_rectangle(world)
        if not is_vector:
            self.destination = point
            point = formation_rectangle.vector_to_point(point)
        else:
            self.destination = self.coordinates(world) + point
        move_rectangle_formation_process = MoveRectangleFormationProcess(formation_rectangle, self.vehicle_types,
                                                                         point, self.group,
                                                                         self.speed * 0.6)
        move_rectangle_formation_process.is_finished = lambda player, world: len(
            move_rectangle_formation_process.actions) == 0
        add_process_to_queue(move_rectangle_formation_process, None)

    def escape(self, me: Player, world: World, game: Game, move: Move, point):
        def return_back():
            def set_state_none():
                self.state = FormationState.NONE

            process = SpreadFormationProcess(self.formation_rectangle(world), self.vehicle_types, self.group, 0.1,
                                             self.escape_point)
            process.is_finished = lambda player, world: world.tick_index - self.escape_tick > 60
            add_process_to_queue(process, set_state_none)

        self.state = FormationState.ESCAPE
        self.escape_point = point
        self.escape_tick = world.tick_index
        process = SpreadFormationProcess(self.formation_rectangle(world), self.vehicle_types, self.group, 10, point)
        process.is_finished = lambda player, world: world.tick_index - self.escape_tick > 30
        add_process_to_queue(process, return_back)

    def distant_to(self, point):
        distant = 1000000
        for vehicle in get_vehicles(self.units).values():
            distance_to = vehicle.get_distance_to(point.x, point.y)
            if distance_to < distant:
                distant = distance_to
        return distant

    def move_if_need(self, me: Player, world: World, game: Game, move: Move, point):
        distant = self.distant_to(point)
        if distant > self.max_range:
            self.move(me, world, game, move, point)
        elif distant < self.min_range:
            self.move(me, world, game, move, (self.coordinates(world) - point) * 10, is_vector=True)

    def formation_rectangle(self, world):
        raise NotImplemented

    def look_vector(self, world):
        raise NotImplemented

    def coordinates(self, world):
        return self.formation_rectangle(world).center()

    def nearest(self, enemy_ids, world: World):
        distant = world.width * world.height
        point = Point(0, 0)
        coordinates = self.coordinates(world)
        nearest_enemy_id = None
        for enemy in get_vehicles(enemy_ids).values():
            if enemy.get_distance_to(coordinates.x, coordinates.y) < distant:
                distant = enemy.get_distance_to(coordinates.x, coordinates.y)
                point.x = enemy.x
                point.y = enemy.y
                nearest_enemy_id = enemy.id
        return point, nearest_enemy_id

    def angle_to_point(self, world, point):
        look_vector = self.look_vector(world)
        look_vector = numpy.array([look_vector.x, look_vector.y], dtype=float)
        vector_to_point = point - self.coordinates(world)
        vector_to_point = numpy.array([vector_to_point.x, vector_to_point.y], dtype=float)
        return atan2(numpy.cross(look_vector, vector_to_point), numpy.dot(look_vector, vector_to_point))

    def is_enemy_in_fire_range(self, enemy):
        for vehicle in get_vehicles(self.units).values():
            if vehicle.get_distance_to_unit(enemy) < vehicle.aerial_attack_range or vehicle.get_distance_to_unit(
                    enemy) < vehicle.ground_attack_range:
                return True
        return False

    def gunner(self, point):
        distant = 1000000000
        gunner = None
        for vehicle in get_vehicles(self.units).values():
            if vehicle.get_distance_to(point.x, point.y) < distant:
                distant = vehicle.get_distance_to(point.x, point.y)
                gunner = vehicle.id
        return gunner

    def is_enemy_visible(self, enemy):
        for vehicle in get_vehicles(self.units).values():
            pass

    def rotate(self, me: Player, world: World, game: Game, move: Move, angle):
        self.state = FormationState.ROTATE

        def rotate():
            def spread():
                def set_state_none():
                    self.state = FormationState.NONE

                add_process_to_queue(
                    SpreadFormationProcess(self.formation_rectangle(world), self.vehicle_types, self.group, 0.8), set_state_none)

            add_process_to_queue(RotateFormationProcess(self.formation_rectangle(world), self.vehicle_types, self.group, angle),
                                 spread)

        add_process_to_queue(SpreadFormationProcess(self.formation_rectangle(world), self.vehicle_types, self.group, 1.1),
                             rotate)


class AviationFormation(Formation):
    def __init__(self, units, game, world):
        super().__init__(units, world, [VehicleType.HELICOPTER, VehicleType.FIGHTER], 1, game.helicopter_speed,
                         game.fighter_aerial_attack_range, game.fighter_vision_range)
        self.is_fighters_on_position = False
        self.is_helicopter_on_position = False

    def formation_rectangle(self, world):
        return Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                       [VehicleType.FIGHTER, VehicleType.HELICOPTER], world)

    def look_vector(self, world):
        fighters_rectangle_center = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                            VehicleType.FIGHTER, world).center()
        helicopters_rectangle_center = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                               VehicleType.HELICOPTER, world).center()

        return Point(fighters_rectangle_center.x - helicopters_rectangle_center.x,
                     fighters_rectangle_center.y - helicopters_rectangle_center.y)

    def format(self, me: Player, world: World, game: Game, move: Move):
        fighters_rectangle = Rectangle.calculate_formation_rectangle(get_vehicles(self.units), VehicleType.FIGHTER,
                                                                     world)
        helicopters_rectangle = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                        VehicleType.HELICOPTER, world)
        fighters_rectangle_center = fighters_rectangle.center()
        helicopters_rectangle_center = helicopters_rectangle.center()
        self.state = FormationState.FORMAT

        def fighters_on_position():
            self.is_fighters_on_position = True
            if self.is_helicopter_on_position:
                close_up()

        def helicopter_on_position():
            self.is_helicopter_on_position = True
            if self.is_fighters_on_position:
                close_up()

        def close_up():

            def assign():
                add_process_to_queue(AssignToGroup(self.formation_rectangle(world),
                                                   self.vehicle_types,
                                                   self.group),
                                     set_state_none)

            def set_state_none():
                self.state = FormationState.NONE

            helicopters_rectangle = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                            VehicleType.HELICOPTER, world)

            add_process_to_queue(
                MoveRectangleFormationProcess(
                    helicopters_rectangle, VehicleType.HELICOPTER,
                    self.look_vector(world),
                    None
                ),
                assign)

        if fighters_rectangle_center.x == helicopters_rectangle_center.x:
            add_process_to_queue(SplitRectangleFormationProcess(fighters_rectangle, VehicleType.FIGHTER, "right",
                                                                get_vehicle(self.units[1]).radius),
                                 fighters_on_position)
            add_process_to_queue(SplitRectangleFormationProcess(helicopters_rectangle, VehicleType.HELICOPTER, "right",
                                                                get_vehicle(self.units[1]).radius),
                                 helicopter_on_position)
        else:
            add_process_to_queue(SplitRectangleFormationProcess(fighters_rectangle, VehicleType.FIGHTER, "bottom",
                                                                get_vehicle(self.units[1]).radius),
                                 fighters_on_position)
            add_process_to_queue(SplitRectangleFormationProcess(helicopters_rectangle, VehicleType.HELICOPTER, "bottom",
                                                                get_vehicle(self.units[1]).radius),
                                 helicopter_on_position)


class GroundFormation(Formation):
    def __init__(self, units, game, world, positions):
        super().__init__(units, world, [VehicleType.TANK, VehicleType.ARRV, VehicleType.IFV], 2, game.tank_speed,
                         0, game.tank_ground_attack_range / 2)
        self.positions = positions
        self.done = False
        self.angle = numpy.math.pi / 2

    def format(self, me: Player, world: World, game: Game, move: Move, last=None):
        self.state = FormationState.FORMAT
        units = get_vehicles(self.units)
        vehicle_types = self.vehicle_types.copy()
        vehicle_types.remove(last)
        unit_radius = get_vehicle(self.units[1]).radius
        factor = 2.9
        offset = unit_radius * 3

        def spread_in():
            def set_state_none():
                self.state = FormationState.NONE
            rectangle = self.formation_rectangle(world)
            rectangle.x1 += offset
            add_process_to_queue(MoveRectangleFormationProcess(
                rectangle, self.vehicle_types,
                Point(-world.width, 0), None),
                lambda: add_process_to_queue(AssignToGroup(self.formation_rectangle(world),
                                                           self.vehicle_types,
                                                           self.group),
                                             set_state_none))

        def close_up():
            if self.done:
                add_process_to_queue(MoveRectangleFormationProcess(
                    Rectangle.calculate_formation_rectangle(units, last, world),
                    last, Point(
                        0,
                        Rectangle.calculate_formation_rectangle(units, vehicle_types[1], world).center().y -
                        Rectangle.calculate_formation_rectangle(units, last, world).center().y - offset),
                    None),
                    lambda: add_process_to_queue(
                        MoveRectangleFormationProcess(
                            Rectangle.calculate_formation_rectangle(units, last, world),
                            last, Point(0-offset, 0), None),

                    spread_in))

        def set_done():
            if not self.done:
                self.done = True
            else:
                close_up()

        def inline_and_spread_last():
            add_process_to_queue(
                MoveRectangleFormationProcess(Rectangle.calculate_formation_rectangle(units, vehicle_types[0], world),
                                              vehicle_types[0], Point(0, unit_radius * 3), None), lambda: add_process_to_queue(MoveRectangleFormationProcess(
                    Rectangle.calculate_formation_rectangle(units, vehicle_types[0], world),
                    vehicle_types[0], Rectangle.calculate_formation_rectangle(units, vehicle_types[0], world).vector_to_point(
                        Rectangle.calculate_formation_rectangle(units, vehicle_types[1], world).center() + Point(0, offset)),
                    None), set_done))

            last_rectangle = Rectangle.calculate_formation_rectangle(units, last, world)
            if last_rectangle.center().x < 74:
                x = 0
                if last_rectangle.center().y > 180:
                    y = last_rectangle.y2
            else:
                x = last_rectangle.center().x
            if last_rectangle.center().y < 74:
                y = 0
            else:
                y = last_rectangle.center().y

            add_process_to_queue(SpreadFormationProcess(
                last_rectangle,
                last, None, factor, Point(x, y)), lambda: add_process_to_queue(MoveRectangleFormationProcess(
                    Rectangle.calculate_formation_rectangle(units, last, world),
                    last,  Rectangle.calculate_formation_rectangle(units, last, world).vector_to_point(Point(
                        Rectangle.calculate_formation_rectangle(units, vehicle_types[1], world).center().x + offset,
                        Rectangle.calculate_formation_rectangle(units, last, world).center().y)),
                    None), set_done))

        rectangle = Rectangle.calculate_formation_rectangle(units, vehicle_types, world)
        add_process_to_queue(SpreadFormationProcess(rectangle, vehicle_types, None, factor, Point(rectangle.center().x, rectangle.y1)), inline_and_spread_last)

    def formation_rectangle(self, world):
        return Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                       self.vehicle_types, world)

    def look_vector(self, world):
        tank_rectangle_center = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                        VehicleType.TANK, world).center()
        ifv_rectangle_center = Rectangle.calculate_formation_rectangle(get_vehicles(self.units),
                                                                       VehicleType.IFV, world).center()

        return Point(tank_rectangle_center.x - ifv_rectangle_center.x,
                     tank_rectangle_center.y - ifv_rectangle_center.y)

    def angle_to_point(self, world, point):
        vector_to_point = numpy.array([point.x, point.y], dtype=float)
        return self.angle - atan2(numpy.cross(norm_vector, vector_to_point), numpy.dot(norm_vector, vector_to_point))

    def rotate(self, me: Player, world: World, game: Game, move: Move, angle):
        self.angle -= angle
        if abs(self.angle) > numpy.math.pi:
            self.angle = (self.angle / self.angle) * (abs(self.angle) - numpy.math.pi)
        super().rotate(me, world, game, move, angle)

