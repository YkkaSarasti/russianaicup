from math import hypot


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def __repr__(self):
        return self.__str__()

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __truediv__(self, other):
        return Point(self.x / other, self.y / other)

    def __mul__(self, other):
        return Point(self.x * other, self.y * other)

    def distant_to(self, point):
        return hypot(point.x - self.x, point.y - self.y)


class Rectangle:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def square(self):
        return (self.x2 - self.x1) * (self.y2 - self.y1)

    def __str__(self):
        return "({}, {}) ({}, {})".format(self.x1, self.y1, self.x2, self.y2)

    def __repr__(self):
        return self.__str__()

    def length_x(self):
        return self.x2 - self.x1

    def length_y(self):
        return self.y2 - self.y1

    def center(self):
        return Point((self.x1 + self.x2) / 2, (self.y1 + self.y2) / 2)

    def vector_to_point(self, point):
        return Point(point.x - self.center().x, point.y - self.center().y)

    @staticmethod
    def calculate_formation_rectangle(units, vehicle_types, world):
        if type(vehicle_types) == int:
            vehicle_types = [vehicle_types]
        rectangle = Rectangle(world.width, world.height, 0, 0)
        for unit in units.values():
            if unit.type in vehicle_types:
                x = unit.x
                y = unit.y

                if x < rectangle.x1:
                    rectangle.x1 = x - unit.radius
                if y < rectangle.y1:
                    rectangle.y1 = y - unit.radius

                if x > rectangle.x2:
                    rectangle.x2 = x + unit.radius
                if y > rectangle.y2:
                    rectangle.y2 = y + unit.radius
        return rectangle

    @staticmethod
    def calculate_formation_rectangle_from_center(center, length_x, length_y):
        return Rectangle(center.x - length_x / 2, center.y - length_y / 2, center.x + length_x, center.y + length_y)
