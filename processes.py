from Rectangle import Rectangle, Point
from model.ActionType import ActionType


active_process = None
current_processes = []
process_queue = []
vehicles = {}
updates = []
last_updates = []
two_tick_updates = []


def get_vehicles(ids):
    new_vehicles = {}
    for id in ids:
        if id in vehicles.keys():
            new_vehicles[id] = vehicles[id]
    return new_vehicles


def get_vehicle(id):
    return vehicles[id]


def add_vehicle(new_vehicle):
    global vehicles
    vehicles[new_vehicle.id] = new_vehicle


def remove_vehicle(vehicle_id):
    del vehicles[vehicle_id]


def update_vehicles(vehicle_updates):
    global last_updates
    global updates
    global two_tick_updates
    two_tick_updates = last_updates
    last_updates = updates
    updates = vehicle_updates
    for vehicle_update in vehicle_updates:
        vehicle = vehicles[vehicle_update.id]
        if vehicle_update.durability == 0:
            remove_vehicle(vehicle_update.id)
        else:
            vehicle.x = vehicle_update.x
            vehicle.y = vehicle_update.y
            vehicle.groups = vehicle_update.groups


def rotate_processes(player, world, game, move):
    global active_process

    for process in current_processes:
        if process != active_process and process.is_finished(player, world):
            process.on_finish()
            current_processes.remove(process)

    if active_process is None and len(process_queue) != 0:
        active_process = process_queue.pop(0)
        current_processes.append(active_process)

    if active_process is not None:
        if active_process.can_move():
            active_process.move(player, world, game, move)
        else:
            active_process = None


def add_process_to_queue(process, on_finish):
    if on_finish is not None:
        process.on_finish = on_finish
    process_queue.append(process)


def create_select_rectangle_procedure(vehicle_type, rectangle, action_type):
    def select_rectangle(player, world, game, move):
        move.action = action_type
        move.top = rectangle.y1
        move.right = rectangle.x2
        move.bottom = rectangle.y2
        move.left = rectangle.x1
        move.vehicle_type = vehicle_type

    return select_rectangle


def create_select_group_procedure(group):
    def select_group(player, world, game, move):
        move.action = ActionType.CLEAR_AND_SELECT
        move.group = group

    return select_group


class Process:
    def __init__(self, vehicle_type):
        self.actions = []
        self.vehicle_type = vehicle_type

    def move(self, player, world, game, move):
        self.actions.pop(0)(player, world, game, move)

    def can_move(self):
        return len(self.actions) != 0

    def is_finished(self, player, world):
        global vehicles
        if type(self.vehicle_type) == int:
            vehicle_type = [self.vehicle_type]
        else:
            vehicle_type = self.vehicle_type
        for vehicle in world.vehicle_updates:
            my_vehicle = vehicles.get(vehicle.id)
            if my_vehicle is not None and my_vehicle.type in vehicle_type and my_vehicle.player_id == player.id:
                return False
        return True


class MoveRectangleFormationProcess(Process):
    def __init__(self, rectangle: Rectangle, vehicle_type, vector, group, max_speed=None):
        super().__init__(vehicle_type)

        def move(player, world, game, move):
            move.action = ActionType.MOVE
            move.x = vector.x
            move.y = vector.y
            if max_speed is not None:
                move.max_speed = max_speed

        if group is not None:
            self.actions.append(create_select_group_procedure(group))
        else:
            if type(vehicle_type) == list:
                self.actions.append(
                    create_select_rectangle_procedure(vehicle_type[0], rectangle, ActionType.CLEAR_AND_SELECT))
                for vtype in vehicle_type:
                    self.actions.append(
                        create_select_rectangle_procedure(vtype, rectangle, ActionType.ADD_TO_SELECTION))
            else:
                self.actions.append(create_select_rectangle_procedure(vehicle_type, rectangle, ActionType.CLEAR_AND_SELECT))
        self.actions.append(move)

    def on_finish(self):
        pass


class SplitRectangleFormationProcess(Process):
    def __init__(self, rectangle: Rectangle, vehicle_type, side, unit_radius):
        super().__init__(vehicle_type)
        self.finished = False

        def finish():
            self.finished = True

        def inline_right():
            moving_rect = Rectangle(split_rectangle.x1 + offset, split_rectangle.y1,
                                    split_rectangle.x2 + offset, split_rectangle.y2)
            add_process_to_queue(MoveRectangleFormationProcess(
                moving_rect,
                vehicle_type, moving_rect.vector_to_point(
                    Point(moving_rect.center().x,
                          moving_rect.center().y + moving_rect.length_y() + unit_radius / 2)), None), finish)

        def inline_bottom():
            moving_rect = Rectangle(split_rectangle.x1, split_rectangle.y1 + offset,
                                    split_rectangle.x2, split_rectangle.y2 + offset)
            add_process_to_queue(MoveRectangleFormationProcess(
                moving_rect,
                vehicle_type, moving_rect.vector_to_point(
                    Point(moving_rect.center().x - moving_rect.length_x() - unit_radius / 2,
                          moving_rect.center().y)), None), finish)

        if side == "right":
            offset = rectangle.length_x() + unit_radius
            point = Point(offset, 0)
            split_rectangle = Rectangle(rectangle.x1, rectangle.y1, rectangle.x2, rectangle.y1 + rectangle.length_y()/2)
            add_process_to_queue(MoveRectangleFormationProcess(
                split_rectangle,
                vehicle_type, point, None), inline_right)
        elif side == "bottom":
            offset = rectangle.length_y() + unit_radius
            point = Point(0, offset)
            split_rectangle = Rectangle(rectangle.x1 + rectangle.length_x()/2, rectangle.y1, rectangle.x2, rectangle.y2)
            add_process_to_queue(MoveRectangleFormationProcess(
                split_rectangle,
                vehicle_type, point, None), inline_bottom)

    def is_finished(self, player, world):
        return self.finished

    def on_finish(self):
        pass


class AssignToGroup(Process):
    def __init__(self, rectangle: Rectangle, vehicle_types, group):
        super().__init__(vehicle_types)

        def clear(player, world, game, move):
            move.action = ActionType.CLEAR_AND_SELECT
            move.top = 0
            move.right = 0
            move.bottom = 0
            move.left = 0
            move.vehicle_type = vehicle_types[0]

        self.actions.append(clear)

        for vehicle_type in vehicle_types:
            self.actions.append(create_select_rectangle_procedure(vehicle_type, rectangle, ActionType.ADD_TO_SELECTION))

        def assign(player, world, game, move):
            move.action = ActionType.ASSIGN
            move.group = group

        self.actions.append(assign)

    def is_finished(self, player, world):
        return len(self.actions) == 0

    def on_finish(self):
        pass


class SpreadFormationProcess(Process):
    def __init__(self, rectangle: Rectangle, vehicle_type, group, factor, point=None):
        super().__init__(vehicle_type)

        def spread(player, world, game, move):
            move.action = ActionType.SCALE
            if point is not None:
                move.x = point.x
                move.y = point.y
            else:
                move.x = rectangle.center().x
                move.y = rectangle.center().y
            move.factor = factor
            move.max_speed = game.helicopter_speed * 0.6

        if group is not None:
            self.actions.append(create_select_group_procedure(group))
        else:
            if type(vehicle_type) == list:
                self.actions.append(
                    create_select_rectangle_procedure(vehicle_type[0], rectangle, ActionType.CLEAR_AND_SELECT))
                for vtype in vehicle_type:
                    self.actions.append(
                        create_select_rectangle_procedure(vtype, rectangle, ActionType.ADD_TO_SELECTION))
            else:
                self.actions.append(create_select_rectangle_procedure(vehicle_type, rectangle, ActionType.CLEAR_AND_SELECT))
        self.actions.append(spread)

    def on_finish(self):
        pass


class RotateFormationProcess(Process):
    def __init__(self, rectangle: Rectangle, vehicle_type, group, angle):
        super().__init__(vehicle_type)

        def rotate(player, world, game, move):
            move.action = ActionType.ROTATE
            move.x = rectangle.center().x
            move.y = rectangle.center().y
            move.angle = angle
            move.max_angular_speed = game.helicopter_speed * 0.6

        self.actions.append(create_select_group_procedure(group))
        self.actions.append(rotate)


class NuclearStrike:
    def __init__(self, unit_id, point):
        self.actions = []
        self.start_tick = 0
        self.strike_delay = 0

        def strike(player, world, game, move):
            move.action = ActionType.TACTICAL_NUCLEAR_STRIKE
            move.vehicle_id = unit_id
            move.x = point.x
            move.y = point.y
            self.start_tick = world.tick_index
            self.strike_delay = game.tactical_nuclear_strike_delay

        self.actions.append(strike)

    def can_move(self):
        return len(self.actions) != 0

    def move(self, player, world, game, move):
        self.actions.pop(0)(player, world, game, move)

    def is_finished(self, player, world):
        return self.start_tick + self.strike_delay >= world.tick_index

    def on_finish(self):
        pass
