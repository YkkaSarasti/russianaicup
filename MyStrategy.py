import math

from model.VehicleType import VehicleType
from model.Game import Game
from model.Move import Move
from model.Player import Player
from model.World import World
from model.Vehicle import Vehicle
from formations import FormationState, AviationFormation, GroundFormation
from processes import rotate_processes, add_vehicle, add_process_to_queue, update_vehicles, get_vehicles, \
    MoveRectangleFormationProcess, get_vehicle, NuclearStrike, SpreadFormationProcess
from Rectangle import Point, Rectangle

formations = {}
enemy_ids = []
START_FORMATION_OFFSET = 74
last_update_tick = 0
opponent_nuclear_strike_tick = -1


def get_far_formation(units, vehicle_types, world):
    distant = 0
    far_vehicle_type = None
    point = Point(0, 0)
    for vehicle_type in vehicle_types:
        distant_to = Rectangle.calculate_formation_rectangle(units, vehicle_type, world).center().distant_to(
            point)
        if distant_to > distant:
            distant = distant_to
            far_vehicle_type = vehicle_type
    return far_vehicle_type


class MyStrategy:
    global formations

    def move(self, me: Player, world: World, game: Game, move: Move):
        def create_aviation_formation():
            aviation_formation = AviationFormation(aviation_ids, game, world)
            formations["aviation"] = aviation_formation
            aviation_formation.format(me, world, game, move)

        def inline_ground(vehicle_types):
            units = get_vehicles(ground_ids)
            vehicle_type = get_far_formation(units, vehicle_types, world)

            if len(vehicle_types) == 3:
                formations["ground"] = GroundFormation(ground_ids, game, world, list(vehicle_types))

            if len(vehicle_types) > 1:
                rectangle = Rectangle.calculate_formation_rectangle(units, vehicle_type, world)
                vehicle_types.remove(vehicle_type)
                add_process_to_queue(MoveRectangleFormationProcess(
                    rectangle, vehicle_type, rectangle.vector_to_point(points.pop(0)), None), lambda: inline_ground(vehicle_types))
            else:
                formations["ground"].format(me, world, game, move, vehicle_types.pop())

        def inline_aviation():
            aviation = get_vehicles(aviation_ids)
            fighters_rectangle = Rectangle.calculate_formation_rectangle(aviation, VehicleType.FIGHTER, world)
            helicopters_rectangle = Rectangle.calculate_formation_rectangle(aviation, VehicleType.HELICOPTER, world)
            fighters_rectangle_center = fighters_rectangle.center()
            helicopters_rectangle_center = helicopters_rectangle.center()

            if fighters_rectangle_center.x == helicopters_rectangle_center.x or \
                    fighters_rectangle_center.y == helicopters_rectangle_center.y:
                create_aviation_formation()
                point = None
            elif helicopters_rectangle_center.y < fighters_rectangle_center.y:
                point = Point(fighters_rectangle_center.x, fighters_rectangle_center.y-START_FORMATION_OFFSET)
            elif helicopters_rectangle_center.y > fighters_rectangle_center.y:
                point = Point(fighters_rectangle_center.x, fighters_rectangle_center.y+START_FORMATION_OFFSET)
            elif helicopters_rectangle_center.x < fighters_rectangle_center.x:
                point = Point(fighters_rectangle_center.x-START_FORMATION_OFFSET, fighters_rectangle_center.x)
            elif helicopters_rectangle_center.x > fighters_rectangle_center.x:
                point = Point(fighters_rectangle_center.x+START_FORMATION_OFFSET, fighters_rectangle_center.x)

            if point is not None:
                add_process_to_queue(
                    MoveRectangleFormationProcess(
                        helicopters_rectangle, VehicleType.HELICOPTER,
                        helicopters_rectangle.vector_to_point(
                            point), None), lambda: create_aviation_formation())

        rotate_processes(me, world, game, move)
        update_vehicles(world.vehicle_updates)

        if world.tick_index == 0:
            aviation_ids = []
            ground_ids = []
            for vehicle in world.new_vehicles:
                add_vehicle(vehicle)
                if vehicle.player_id != me.id:
                    enemy_ids.append(vehicle.id)
                if vehicle.type in [VehicleType.FIGHTER, VehicleType.HELICOPTER] and vehicle.player_id == me.id:
                    aviation_ids.append(vehicle.id)
                if vehicle.type in [VehicleType.ARRV, VehicleType.IFV, VehicleType.TANK] and vehicle.player_id == me.id:
                    ground_ids.append(vehicle.id)

            inline_aviation()
            points = [Point(270, 250), Point(180, 250)]
            inline_ground([VehicleType.ARRV, VehicleType.IFV, VehicleType.TANK])

        aviation_formation = formations.get("aviation")
        ground_formation = formations.get("ground")

        if world.get_opponent_player().next_nuclear_strike_tick_index != -1:
            point = Point(world.get_opponent_player().next_nuclear_strike_x,
                          world.get_opponent_player().next_nuclear_strike_y)
            if aviation_formation is not None \
                    and aviation_formation.state != FormationState.ESCAPE \
                    and aviation_formation.distant_to(point) < game.tactical_nuclear_strike_radius:
                aviation_formation.escape(me, world, game, move, point)
            if ground_formation is not None \
                    and ground_formation.state != FormationState.ESCAPE \
                    and ground_formation.distant_to(point) < game.tactical_nuclear_strike_radius:
                ground_formation.escape(me, world, game, move, point)

        global last_update_tick
        is_need_update = world.tick_index - last_update_tick >= 30

        if aviation_formation is not None and world.get_opponent_player().next_nuclear_strike_tick_index == -1:
            if aviation_formation.state in [FormationState.NONE, FormationState.MOVE] and is_need_update:
                nearest_enemy_point, nearest_enemy_id = aviation_formation.nearest(enemy_ids, world)
                angle_to_nearest_enemy = aviation_formation.angle_to_point(world, nearest_enemy_point)

                if me.remaining_nuclear_strike_cooldown_ticks == 0 \
                        and game.fighter_vision_range > aviation_formation.distant_to(
                            nearest_enemy_point) > game.tactical_nuclear_strike_radius / 4:
                    add_process_to_queue(
                        NuclearStrike(aviation_formation.gunner(nearest_enemy_point), nearest_enemy_point), None)

                if math.fabs(angle_to_nearest_enemy) > 0.5:
                    aviation_formation.rotate(me, world, game, move, angle_to_nearest_enemy)
                else:
                    aviation_formation.move_if_need(me, world, game, move, nearest_enemy_point)

        if ground_formation is not None and world.get_opponent_player().next_nuclear_strike_tick_index == -1:
            if ground_formation.state in [FormationState.NONE, FormationState.MOVE] and is_need_update:
                nearest_enemy_point, nearest_enemy_id = ground_formation.nearest(enemy_ids, world)
                angle_to_nearest_enemy = ground_formation.angle_to_point(world, nearest_enemy_point)

                if ground_formation.distant_to(nearest_enemy_point) < game.tank_ground_attack_range:
                    aviation_formation.min_range = 0
                    aviation_formation.max_range = game.helicopter_ground_attack_range / 4

                if math.fabs(angle_to_nearest_enemy) > 0.5:
                    ground_formation.rotate(me, world, game, move, angle_to_nearest_enemy)
                else:
                    ground_formation.move_if_need(me, world, game, move, nearest_enemy_point)

        if is_need_update:
            last_update_tick = world.tick_index
